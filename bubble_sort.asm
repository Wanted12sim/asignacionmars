.data
array:
        .word 0 : 100
prompt_length_str:
        .asciiz "Cantidad de numeros a ingresar (de 0 a 100): "
prompt_length_wrong_str:
        .asciiz "Cantidad de numeros incorrecta. Intentalo de nuevo\n"
prompt_number_str:
        .asciiz "Ingrese el numero: "

newline_str:
        .asciiz "\n"
array_unordered_str:
        .asciiz "Arreglo sin ordenar: "
array_ordered_str:
        .asciiz "Arreglo ordenado: "

println_arr_begin_str:
        .asciiz "["
println_arr_sep_str:
        .asciiz ", "
println_arr_end_str:
        .asciiz "]\n"

.text
prompt_length_start:
        li $v0, 4
        la $a0, prompt_length_str
        syscall

        li $v0, 5
        syscall

        li $t0, -1
        slt $t1, $t0, $v0
        beq $t1, $zero, prompt_length_wrong

        li $t0, 101
        slt $t1, $v0, $t0
        beq $t1, $zero, prompt_length_wrong

        j prompt_length_success
prompt_length_wrong:
        li $v0, 4
        la $a0, prompt_length_wrong_str
        syscall
        j prompt_length_start

prompt_length_success:
        move $s0, $v0
        move $t0, $zero

prompt_number:
        slt $t1, $t0, $s0
        beq $t1, $zero, prompt_number_end

        li $v0, 4
        la $a0, prompt_number_str
        syscall

        li $v0, 5
        syscall

        la $t2, array
        sll $t1, $t0, 2
        add $t1, $t1, $t2
        sw $v0, 0($t1)

        addi $t0, $t0, 1
        j prompt_number
prompt_number_end:
        li $v0, 4
        la $a0, newline_str
        syscall

        li $v0, 4
        la $a0, array_unordered_str
        syscall

        la $a0, array
        move $a1, $s0
        jal println_arr

        la $a0, array
        move $a1, $s0
        jal bubble_sort

        li $v0, 4
        la $a0, array_ordered_str
        syscall

        la $a0, array
        move $a1, $s0
        jal println_arr

        li $v0, 10
        syscall

# a0: arr
# a1: len
println_arr:
        move $t1, $a0

        li $v0, 4
        la $a0, println_arr_begin_str
        syscall

        slt $t0, $zero, $a1
        beq $t0, $zero, println_arr_end

        li $v0, 1
        lw $a0, 0($t1)
        syscall

        addi $t1, $t1, 4
        addi $a1, $a1, -1
println_arr_loop:
        slt $t0, $zero, $a1
        beq $t0, $zero, println_arr_end

        li $v0, 4
        la $a0, println_arr_sep_str
        syscall

        li $v0, 1
        lw $a0, 0($t1)
        syscall

        addi $t1, $t1, 4
        addi $a1, $a1, -1
        j println_arr_loop
println_arr_end:
        li $v0,  4
        la $a0, println_arr_end_str
        syscall
        jr $ra

# a0: arr
# a1: len
bubble_sort:
        # i = 0
        move $t0, $zero
        outer_loop:
                # verificar i < len
                slt $t7, $t0, $a1
                beq $t7, $zero, outer_loop_end

                # j = i + 1
                addi $t1, $t0, 1
                inner_loop:
                        # verificar j < len
                        slt $t7, $t1, $a1
                        beq $t7, $zero, inner_loop_end

                        # cargar arr[i] en t3
                        sll $t7, $t0, 2
                        add $t7, $t7, $a0
                        lw $t3, 0($t7)

                        # cargar arr[j] en t4
                        sll $t7, $t1, 2
                        add $t7, $t7, $a0
                        lw $t4, 0($t7)

                        # ignorar si se cumple arr[i] < arr[j]
                        slt $t7, $t3, $t4
                        bne $t7, $zero, inner_loop_continue

                        # intercambiar $t3 por $t4 y vice-versa

                        # guardar t3 en arr[j]
                        sll $t7, $t0, 2
                        add $t7, $t7, $a0
                        sw $t4, 0($t7)

                        # guardar t4 en arr[i]
                        sll $t7, $t1, 2
                        add $t7, $t7, $a0
                        sw $t3, 0($t7)

                        inner_loop_continue:
                                addi $t1, $t1, 1
                                j inner_loop
                inner_loop_end:
                        addi $t0, $t0, 1
                        j outer_loop
        outer_loop_end:
                jr $ra
